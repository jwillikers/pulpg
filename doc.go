// Package pulpg simplifies pulling static or dynamic html content from web pages.
//
// There is a simple Puller interface for "pulling" content from a given url.
package pulpg
