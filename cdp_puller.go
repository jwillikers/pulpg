package pulpg

import (
	"context"

	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/client"
	"github.com/chromedp/chromedp/runner"
)

// CDP is an interface for masking chromedp. It allows for decoupling the implementation from the code base, at least a little.
type CDP interface {
	Run(context.Context, chromedp.Action) error
	Shutdown(context.Context, ...client.Option) error
	Wait() error
}

// CdpPuller represents a web scraper that uses chromedp to dynamically load and pull html from a web site.
type CdpPuller struct {
	chrome CDP
	url    string
	sel    string
}

// NewCdp is a convenience function which handles creating a new CDP instance.
func NewCdp(ctx context.Context) (CDP, error) {
	return chromedp.New(ctx, chromedp.WithRunnerOptions(
		runner.Flag("disable-web-security", true),
		runner.Flag("no-first-run", true),
		runner.Flag("no-default-browser-check", true),
		runner.Flag("disable-gpu", true),
	))
}

// Navigate loads the web page at the given url using the chromedp instance of the CdpPuller. If the web page is loaded successfully, the CDP's url is updated.
func (p *CdpPuller) navigate(ctx context.Context, url string) error {
	err := p.chrome.Run(ctx, chromedp.Tasks{
		chromedp.Navigate(url),
	})
	if err == nil {
		p.url = url
	}
	return err
}

// NewCdpPuller creates a new instance of a CdpPuller with the given CDP instance and html selector string.
func NewCdpPuller(chrome CDP, sel string) *CdpPuller {
	return &CdpPuller{chrome, "", sel}
}

// Extract wraps the CDP functionality to obtain a web page's html.
func (p *CdpPuller) extract(ctx context.Context, sel string, html *string) error {
	return p.chrome.Run(ctx, chromedp.Tasks{
		chromedp.InnerHTML(sel, html, chromedp.NodeVisible, chromedp.ByID),
	})
}

// Pull extracts dynamically loaded content from the web page at the given url. It uses the CdpPuller's selector attribute to parse the HTML content. The rest of the chromedp options are hidden from the user.
func (p *CdpPuller) Pull(ctx context.Context, url string) (string, error) {
	var err error
	html := ""
	if p.url != url {
		err = p.navigate(ctx, url)
		if err != nil {
			return html, err
		}
	}
	err = p.extract(ctx, p.sel, &html)
	return html, err
}

// Shutdown closes the Cdp Puller's running chromedp instance. A forced wait is included to ensure the chromedp instance has completely shut down.
func (p *CdpPuller) Shutdown(ctx context.Context) error {
	err := p.chrome.Shutdown(ctx)
	if err != nil {
		err = p.chrome.Wait()
	}
	return err
}
