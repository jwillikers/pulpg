pulpg
===
pulpg, meaning "pull page," is a simple wrapping abstraction and implementation for pulling either statically or dynamically loaded html from web pages.

Getting Started
---
Puller is an interface for pulling the html from a webpage.

### How It Works
There is a [Puller](./puller.go) interface with a single function, Pull. This function returns a string, the html of the web page.

Currently, there is only one implemented puller, [CdpPuller](./cdp_puller.go), which lightly wraps chromedp in order to pull a dynamically populated web page.

A static implementation using go's net library and more dynamic scrapers based off of selenium, phantom-js, and the like will, with luck, be added to allow for more options.

### In the Wild
-   An example that currently uses pulpg's Puller interface is the [scrapegoat](https://bitbucket.org/athrun22/scrapegoat/src/develop/) project.
-   An example that uses the CdpPuller implementation is [sirius](https://bitbucket.org/athrun22/sirius/src/develop/) which scrapes dynamically populated currently playing song names and artists from a SiriusXM channel.

Built With
---
-   [chromedp](https://github.com/chromedp/chromedp) - a faster, simpler way to drive browsers without external dependencies

Versioning
---
[SemVer](http://semver.org/) is used for versioning. For the versions available, see the tags on this repository.

Contributing
---
Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

Authors
---
-   Jordan Williams

License
---
This project is licensed under the MIT License - see the [LICENSE.md](./LICENSE.md) file for details
