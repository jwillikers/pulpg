package pulpg

import "context"

// Puller extracts the content from the web page at the given url.
type Puller interface {
	Pull(context.Context, string) (string, error)
}
