package pulpg

import (
	"context"
	"errors"
	"testing"

	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/client"
)

type MockCDP struct{}

func (c *MockCDP) Wait() error {
	return nil
}

func (c *MockCDP) Shutdown(context.Context, ...client.Option) error {
	return nil
}

func (c *MockCDP) Run(context.Context, chromedp.Action) error {
	return nil
}

type MockCDPError struct{}

func (c *MockCDPError) Wait() error {
	return errors.New("error occured in chromedp wait function")
}

func (c *MockCDPError) Shutdown(context.Context, ...client.Option) error {
	return errors.New("error occured in chromedp shutdown function")
}

func (c *MockCDPError) Run(context.Context, chromedp.Action) error {
	return errors.New("error occured in chromedp run function")
}

func TestNewCdpPuller(t *testing.T) {
	sel := `#onair-pdt`
	p := NewCdpPuller(new(MockCDP), sel)
	if nil == p {
		t.Error("the chromedp Puller should not be nil")
	}
	if nil == p.chrome {
		t.Error("the CDP interface of the Puller should not be nil")
	}
	if "" != p.url {
		t.Errorf("the url of the Puller should be initialized to the empty string not '%s'", p.url)
	}
	if sel != p.sel {
		t.Errorf("the selector of the Puller should be '%s' not '%s'", sel, p.sel)
	}
}

func TestNewCdp(t *testing.T) {
	ctxNewCdp, cancelNewCdp := context.WithCancel(context.Background())
	c, err := NewCdp(ctxNewCdp)
	if nil != err {
		t.Fatalf("the error '%v' should not be returned", err)
	}
	if nil == c {
		t.Error("the chromedp instance should not be nil")
	}
	cancelNewCdp()
	ctxShutdown, cancelShutdown := context.WithCancel(context.Background())
	c.Shutdown(ctxShutdown)
	c.Wait()
	cancelShutdown()
}

func TestNavigate(t *testing.T) {
	t.Run("main", func(t *testing.T) {
		url1 := "www.google.com"
		url2 := "www.gmail.com"
		p := new(CdpPuller)
		p.url = url1
		p.chrome = new(MockCDP)
		ctx, cancel := context.WithCancel(context.Background())
		err := p.navigate(ctx, url2)
		cancel()
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
		if url2 != p.url {
			t.Errorf("the url of the Puller should be '%s' not '%s'", url2, p.url)
		}
	})
}

func TestExtract(t *testing.T) {
	p := new(CdpPuller)
	p.chrome = new(MockCDP)
	html := ""
	ctx, cancel := context.WithCancel(context.Background())
	err := p.extract(ctx, "", &html)
	cancel()
	if nil != err {
		t.Errorf("the error '%v' should not be returned", err)
	}
}

func TestPull(t *testing.T) {
	t.Run("main", func(t *testing.T) {
		p := new(CdpPuller)
		p.chrome = new(MockCDP)
		ctx, cancel := context.WithCancel(context.Background())
		_, err := p.Pull(ctx, "url")
		cancel()
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
	})
	t.Run("error=navigate", func(t *testing.T) {
		p := new(CdpPuller)
		p.chrome = new(MockCDPError)
		ctx, cancel := context.WithCancel(context.Background())
		_, err := p.Pull(ctx, "url")
		cancel()
		if nil == err {
			t.Errorf("there should have been an error")
		}
	})
}

func TestShutdown(t *testing.T) {
	t.Run("main", func(t *testing.T) {
		p := new(CdpPuller)
		p.chrome = new(MockCDP)
		ctx, cancel := context.WithCancel(context.Background())
		err := p.Shutdown(ctx)
		cancel()
		if nil != err {
			t.Errorf("the error '%v' should not be returned", err)
		}
	})
	t.Run("error=shutdown", func(t *testing.T) {
		p := new(CdpPuller)
		p.chrome = new(MockCDPError)
		ctx, cancel := context.WithCancel(context.Background())
		err := p.Shutdown(ctx)
		cancel()
		if nil == err {
			t.Error("an error should have been returned by chromedp")
		}
	})
}
